package deniardcompany.hyips.Additional;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by EPS on 17.05.2016.
 */
public final class ApplicationProgram extends MultiDexApplication implements Application.ActivityLifecycleCallbacks{

    public static DatabaseReference mDatabase;
    public static SubHyip hyip;
    public static String curTable;

    public static final int TOARCHIVE = 1223;
    public static final int CHANGE = 1224;
    public static final int SHOW = 1225;
    public static final int DELETE = 1226;
    public static Context mContext;
    public static Activity mActivity;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        registerActivityLifecycleCallbacks(this);
        JodaTimeAndroid.init(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        mActivity = activity;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        unregisterActivityLifecycleCallbacks(this);
    }
}
