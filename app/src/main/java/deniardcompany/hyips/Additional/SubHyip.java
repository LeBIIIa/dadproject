package deniardcompany.hyips.Additional;


/**
 * Created by LeBIIIa on 30.07.2016.
 */

public class SubHyip  {
    private Hyip hyip;
    private long mDaysLeft;
    private long mDaysToPlus;
    private String mStrNextPay, mSDaysToPlus, mSAgeDeposit, mSCountPay, mStartEnd;

    public SubHyip(Hyip hyip){
        this.hyip = hyip;
    }

    public Hyip getHyip() { return hyip; }
    public final void setHyip(Hyip hyip) { this.hyip = hyip; }

    public String getmStrNextPay() { return mStrNextPay; }
    public final void setmStrNextPay(String mStrNextPay) { this.mStrNextPay = mStrNextPay; }

    public long getmDaysLeft() { return mDaysLeft; }
    public final void setmDaysLeft(long DaysLeft) { this.mDaysLeft = DaysLeft; }

    public long getmDaysToPlus() { return mDaysToPlus; }
    public final void setmDaysToPlus(long mDaysToPlus) { this.mDaysToPlus = mDaysToPlus; }

    public String getmSDaysToPlus() { return mSDaysToPlus; }
    public final void setmSDaysToPlus(String mSDaysToPlus) { this.mSDaysToPlus = mSDaysToPlus; }

    public String getmSAgeDeposit() { return mSAgeDeposit; }
    public final void setmSAgeDeposit(String mSAgeDeposit) { this.mSAgeDeposit = mSAgeDeposit; }

    public String getmSCountPay() { return mSCountPay; }
    public final void setmSCountPay(String mSCountPay) { this.mSCountPay = mSCountPay; }

    public String getmStartEnd() { return mStartEnd; }
    public final void setmStartEnd(String mStartEnd) { this.mStartEnd = mStartEnd; }
}
