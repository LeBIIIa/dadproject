package deniardcompany.hyips.Additional;

import android.app.Activity;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.chrono.StrictChronology;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import deniardcompany.hyips.R;

import static deniardcompany.hyips.Additional.ApplicationProgram.curTable;
import static deniardcompany.hyips.Additional.ApplicationProgram.mActivity;
import static deniardcompany.hyips.Additional.ApplicationProgram.mContext;

/**
 * Created by EPS on 12.04.2016.
 */
public class Methods {

    public static void Update(Hyip hyip) throws Exception
    {
        LocalDate end, calTemp;
        int countPay;
        //
        end = LocalDate.now();
        countPay = hyip.getmCountPay();
        //
        if (hyip.getmAgeDeposit() == 0) {
            calTemp = new LocalDate(9999, 12, 31);
            hyip.setTimeEnd(calTemp.toDate());
        }else {
            calTemp = new LocalDate(hyip.getTimeStart().getTime());
            hyip.setTimeEnd(calTemp.plusDays(hyip.getmAgeDeposit()).toDate());
        }

        if ( hyip.getTimeEnd().before(DateTime.now().toDate()) )
            end = new LocalDate(hyip.getTimeEnd().getTime());

        calTemp = new LocalDate(hyip.getTimeStart().getTime());

        if (countPay != 31) { calTemp = calTemp.plusDays(1); }
        else{ calTemp = calTemp.plusDays(calTemp.getChronology().dayOfMonth().getMaximumValue()); }

        double back = 0;
        double business = hyip.getmWorkDay();
        double nonBusiness = hyip.getmNonWorkDay();

        while (calTemp.compareTo(end) <= 0)
        {
            if ( countPay == 0 || countPay == 1 ){
                int day = calTemp.get(DateTimeFieldType.dayOfWeek());
                if ( day == 6 || day == 7 )
                    back += nonBusiness;
                else
                    back += business;
            }else
                back += business;

            if (countPay == 31)
                calTemp = calTemp.plusDays(calTemp.getChronology().dayOfMonth().getMaximumValue());
            else
                if ( countPay != 0 )
                    calTemp = calTemp.plusDays(countPay);
                else
                    calTemp = calTemp.plusDays(1);
        }

        hyip.setNextPay(calTemp.toDate());

        hyip.setmBack(round(back + hyip.getmRefBack(), 2));
        hyip.setmProfit(Math.max(0, round(hyip.getmBack() - hyip.getmDeposit(), 2)));
    }

    public static void Update(SubHyip subHyip) throws Exception
    {
        LocalDate end, calTemp;
        long span;
        double business, nonBusiness;
        double doubTemp;
        int countPay, days = 0;
        //
        countPay = subHyip.getHyip().getmCountPay();
        business = subHyip.getHyip().getmWorkDay();
        nonBusiness = subHyip.getHyip().getmNonWorkDay();
        end = LocalDate.now();
        //

        Update(subHyip.getHyip());
        set(subHyip);


        calTemp = new LocalDate(subHyip.getHyip().getmNextPay());

        span = Days.daysBetween(new LocalDate(subHyip.getHyip().getTimeStart().getTime()), end).getDays();
        if (subHyip.getHyip().getmAgeDeposit() > 0) { subHyip.setmDaysLeft((long)subHyip.getHyip().getmAgeDeposit() - span); }
        else { subHyip.setmDaysLeft(0); }

        if (subHyip.getHyip().getmProfit() > 0)
            subHyip.setmSDaysToPlus("+");
        else {
            doubTemp = subHyip.getHyip().getmDeposit() - subHyip.getHyip().getmBack();
            span = Days.daysBetween(end, calTemp).getDays();
            if ( Double.compare(business, 0.0f) != 0 ) {
                if ( countPay == 1 && Double.compare(business, nonBusiness) != 0 ){
                    while ( Double.compare(doubTemp, 0.0f) > 0 )
                    {
                        int day = calTemp.get(DateTimeFieldType.dayOfWeek());
                        if ( day == 6 || day == 7 )
                            doubTemp -= nonBusiness;
                        else
                            doubTemp -= business;
                        calTemp = calTemp.plusDays(1);
                    }
                }else{
                    days = (int) (doubTemp / business);
                }

            }
            subHyip.setmDaysToPlus(span + days);
            subHyip.setmSDaysToPlus(String.valueOf(subHyip.getmDaysToPlus()));
        }
    }

    private static void set(SubHyip subHyip)
    {
        LocalDate calTemp = new LocalDate(9999, 12, 31);

        Resources res = ApplicationProgram.mContext.getResources();
        String s;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM", new Locale("ru","RU"));

        if (subHyip.getHyip().getTimeEnd().equals(calTemp.toDate())) {
            subHyip.setmSAgeDeposit(res.getString(R.string.inf));
            s = sdf.format(subHyip.getHyip().getmTimeStart());
            subHyip.setmStartEnd(String.format("%s", s));
        } else {
            subHyip.setmSAgeDeposit(String.valueOf(subHyip.getHyip().getmAgeDeposit()));
            s = String.format("%s %s", sdf.format(subHyip.getHyip().getmTimeStart()), sdf.format(subHyip.getHyip().getmTimeEnd()));
            subHyip.setmStartEnd(s);
        }

        if (subHyip.getHyip().getmCountPay() == 1)
            subHyip.setmSCountPay(res.getString(R.string.day));
        else if (subHyip.getHyip().getmCountPay() == 7)
            subHyip.setmSCountPay(res.getString(R.string.week));
        else if (subHyip.getHyip().getmCountPay() == 30)
            subHyip.setmSCountPay(res.getString(R.string.thirty));
        else if (subHyip.getHyip().getmCountPay() == 31)
            subHyip.setmSCountPay(res.getString(R.string.month));
    }
    //

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static void nullableValue(){
        Log.d("NULL", "nullableValue");
        if ( curTable.compareTo(mContext.getString(R.string.hyip)) == 0 ){
            TextView textView = (TextView)mActivity.findViewById(R.id.allDeposit);
            textView.setText("Вложено: 0.00$");
            textView = (TextView)mActivity.findViewById(R.id.allBack);
            textView.setText("Возвращено: 0.00$");
        }else{
            TextView view = (TextView)mActivity.findViewById(R.id.difference);
            view.setText("0.00$");
        }
    }

    public static void updateValue(double deposit, double back){
        Log.d("NULL", "updateValue");
        DecimalFormat df = new DecimalFormat("0.00");
        StringTokenizer tokenizer;
        if ( curTable.compareTo(mContext.getString(R.string.hyip)) == 0 ){
            TextView dep = (TextView)mActivity.findViewById(R.id.allDeposit);
            TextView ret = (TextView)mActivity.findViewById(R.id.allBack);
            String s = dep.getText().toString();
            tokenizer = new StringTokenizer(s, " ");
            tokenizer.nextToken();
            s = tokenizer.nextToken();
            s = s.substring(0, s.length() - 1);
            double convert = 0;
            if ( s != null || s.equals("") ){
                convert = Double.parseDouble(s);
            }
            dep.setText("Вложено: " + df.format(convert + deposit) + "$");
            s = ret.getText().toString();
            tokenizer = new StringTokenizer(s, " ");
            tokenizer.nextToken();
            s = tokenizer.nextToken();
            s = s.substring(0, s.length() - 1);
            convert = 0;
            if ( s!= null || s.equals("") ){
                convert = Double.parseDouble(s);
            }
            ret.setText("Возвращено: " + df.format(convert + back) + "$");
        }else{
            TextView dif = (TextView)mActivity.findViewById(R.id.difference);
            String s = dif.getText().toString();
            s = s.substring(0, s.length() - 1);
            double convert = 0;
            if ( s != null || s.equals("") ){
                convert = Double.parseDouble(s);
            }
            convert -= deposit;
            convert += back;
            dif.setText(df.format(convert) + "$");
            if ( Double.compare(convert, 0.00f) > 0 ){
                dif.setTextColor(ContextCompat.getColor(mContext, R.color.Plus));
            }else if ( Double.compare(convert, 0.00f) < 0 ) {
                dif.setTextColor(ContextCompat.getColor(mContext, R.color.ToolbarMinus));
            }
        }
    }

    public static String deCrypt(String crypt) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {
        final byte[] encryptedBytes = Base64.decode(crypt, Base64.DEFAULT);

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        final byte[] keyBytes = uid.getBytes("UTF-8");

        SecretKeySpec sks = new SecretKeySpec(keyBytes, "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.DECRYPT_MODE, sks);

        final byte[] resultBytes = cipher.doFinal(encryptedBytes);
        return new String(resultBytes);
    }
}
