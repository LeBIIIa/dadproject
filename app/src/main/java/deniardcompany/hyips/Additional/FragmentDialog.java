package deniardcompany.hyips.Additional;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import org.joda.time.LocalDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import deniardcompany.hyips.R;

import static deniardcompany.hyips.Additional.ApplicationProgram.mContext;

/**
 * Created by LeBIIIa on 28.05.2016.
 */

public class FragmentDialog extends DialogFragment implements View.OnClickListener {
    private DatePicker datePicker;
    private EditText mDate;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private static final String Key = "Date";

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_set_date, container);
        datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        LocalDate local = new LocalDate();
        local.plusDays(1);
        datePicker.setMaxDate(local.toDate().getTime());
        datePicker.setMinDate(new LocalDate(2013, 1, 1).toDate().getTime());

        if ( mDate.getText().length() == 0 ){
            datePicker.updateDate(local.getYear(), local.getMonthOfYear() - 1, local.getDayOfMonth());
        }else{
            try {
                Date temp = sdf.parse(mDate.getText().toString());
                local = new LocalDate(temp);
                datePicker.updateDate(local.getYear(), local.getMonthOfYear() - 1, local.getDayOfMonth());
            }catch (ParseException e){}
        }

        Button btn = (Button) view.findViewById(R.id.okey);
        btn.setOnClickListener(this);
        getDialog().setTitle(mContext.getString(R.string.pick_date));

        return view;
    }

    public void onClick(View v) {
        LocalDate local = new LocalDate(datePicker.getYear(), datePicker.getMonth() + 1, datePicker.getDayOfMonth());
        mDate.setText(sdf.format(local.toDate()));
        mDate.setError(null);
        dismiss();
    }

    public static FragmentDialog newInstance(EditText mDate) {
        FragmentDialog f = new FragmentDialog();
        f.mDate = mDate;
        return f;
    }
}
