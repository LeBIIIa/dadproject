package deniardcompany.hyips.Additional;

import android.content.Context;
import android.content.res.Resources;
import android.util.Base64;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import org.joda.time.LocalDate;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import deniardcompany.hyips.R;

import static deniardcompany.hyips.Additional.ApplicationProgram.curTable;
import static deniardcompany.hyips.Additional.ApplicationProgram.mContext;
import static deniardcompany.hyips.Additional.ApplicationProgram.mDatabase;
import static deniardcompany.hyips.Additional.Methods.round;

/**
 * Created by EPS on 12.04.2016.
 */
@IgnoreExtraProperties
public class Hyip implements Serializable {
    private String mId;
    private String mNameHyip;
    private String mSiteHyip;
    private String mSysPayment;
    private String mLogin;
    private String mPassword;
    private Date mTimeStart;
    private Date mTimeEnd;
    private double mPercentPlan;
    private int mCountPay;
    private int mAgeDeposit;
    private double mDeposit;
    private double mRefBack;
    private double mBack;
    private double mProfit;
    private Date mNextPay;

    //
    private double mWorkDay;
    private double mNonWorkDay;

    public Hyip(){}

    public Hyip(String mNameHyip, String siteHyip, String mSysPayment, String mLogin, String mPassword, Date mTimeStart,
                double mPercentPlan, int mCountPay, double mWorkDay, double mNonWorkDay, int mAgeDeposit, double mDeposit, double mRefBack, double mBack, double mProfit, String id){
        this.mNameHyip = mNameHyip;
        this.mSiteHyip = siteHyip;
        this.mSysPayment = mSysPayment;
        this.mLogin = mLogin;
        this.mPassword = mPassword;
        this.mTimeStart = mTimeStart;
        this.mAgeDeposit = mAgeDeposit;
        this.mDeposit = mDeposit;
        this.mPercentPlan = mPercentPlan;
        this.mCountPay = mCountPay;
        this.mRefBack = mRefBack;
        this.mId = id;
        this.mBack = mBack;
        this.mProfit = mProfit;
        this.mWorkDay = mWorkDay;
        this.mNonWorkDay = mNonWorkDay;
        if ( mCountPay != 0 )
            this.mWorkDay = this.mNonWorkDay = round(mDeposit * (mPercentPlan / 100 ), 2);

        LocalDate date = LocalDate.fromDateFields(mTimeStart);
        if (mAgeDeposit == 0 )
            date = new LocalDate(9999, 12, 31);
        else
            date = date.plusDays( mAgeDeposit);

        mTimeEnd = date.toDate();
    }

    public Hyip(String mNameHyip, String siteHyip, String mSysPayment, String mLogin, String mPassword, Date mTimeStart,
                double mPercentPlan, int mCountPay,double mWorkDay, double mNonWorkDay, int mAgeDeposit, double mDeposit, double mRefBack, double mBack, double mProfit) {
        this(mNameHyip, siteHyip, mSysPayment, mLogin, mPassword, mTimeStart, mPercentPlan, mCountPay, mWorkDay, mNonWorkDay, mAgeDeposit, mDeposit, mRefBack, mBack, mProfit, null);
        createID();
    }

    public void createID(){
        mId = mDatabase.child(curTable).push().getKey();
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("mId", mId);
        result.put("mNameHyip", mNameHyip);
        result.put("mSiteHyip", mSiteHyip);
        result.put("mSysPayment", mSysPayment);
        result.put("mLogin", mLogin);
        result.put("mPassword", mPassword);
        result.put("mTimeStart", mTimeStart.getTime());
        result.put("mTimeEnd", mTimeEnd.getTime());
        result.put("mPercentPlan", mPercentPlan);
        result.put("mCountPay", mCountPay);
        result.put("mWorkDay", mWorkDay);
        result.put("mNonWorkDay", mNonWorkDay);
        result.put("mAgeDeposit", mAgeDeposit);
        result.put("mDeposit", mDeposit);
        result.put("mRefBack", mRefBack);
        result.put("mBack", mBack);
        result.put("mRefBack", mRefBack);
        result.put("mProfit", mProfit);
        if (mNextPay != null)
            result.put("mNextPay", mNextPay.getTime());
        return result;
    }

    /**
     * Returns the item id
     */
    public String getmId() {
        return mId;
    }
    public final void setmId(String id) {
        this.mId = id;
    }

    public String getmNameHyip(){
        return mNameHyip;
    }
    public final void setmNameHyip(String mNameHyip) {
        this.mNameHyip = mNameHyip;
    }

    public String getmSiteHyip(){
        return mSiteHyip;
    }
    public final void setmSiteHyip(String siteHyip) {
        this.mSiteHyip = siteHyip;
    }

    public String getmSysPayment() {
        return mSysPayment;
    }
    public final void setmSysPayment(String mSysPayment) {
        this.mSysPayment = mSysPayment;
    }

    public String getmLogin() {
        return mLogin;
    }
    public final void setmLogin(String mLogin) {
        this.mLogin = mLogin;
    }

    public String getmPassword() {
        return mPassword;
    }
    public final void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public double getmPercentPlan() {
        return mPercentPlan;
    }
    public final void setmPercentPlan(double mPercentPlan) {
        this.mPercentPlan = mPercentPlan;
    }

    public int getmCountPay() {
        return mCountPay;
    }
    public final void setmCountPay(int mCountPay) {
        this.mCountPay = mCountPay;
    }

    public int getmAgeDeposit() {
        return mAgeDeposit;
    }
    public final void setmAgeDeposit(int mAgeDeposit) {
        this.mAgeDeposit = mAgeDeposit;
    }

    public double getmDeposit() {
        return mDeposit;
    }
    public final void setmDeposit(double mDeposit) {
        this.mDeposit = mDeposit;
    }

    public double getmRefBack() {
        return mRefBack;
    }
    public final void setmRefBack(double mRefBack) {
        this.mRefBack = mRefBack;
    }

    public double getmBack() { return mBack; }
    public final void setmBack(double mBack) { this.mBack = mBack; }

    public double getmProfit() { return mProfit; }
    public final void setmProfit(double mProfit) { this.mProfit = mProfit; }
    //
    public long getmNextPay() { return mNextPay.getTime(); }
    public final void setmNextPay(long mNextPay){ this.mNextPay = new Date(mNextPay); }

    public long getmTimeStart() {
        return mTimeStart.getTime();
    }
    public final void setmTimeStart(long mTimeStart) {
        this.mTimeStart = new Date(mTimeStart);
    }

    public long getmTimeEnd() {
        return mTimeEnd.getTime();
    }
    public final void setmTimeEnd(long mTimeEnd) {
        this.mTimeEnd = new Date(mTimeEnd);
    }

    public double getmWorkDay() { return mWorkDay; }
    public void setmWorkDay(double mWorkDay) { this.mWorkDay = mWorkDay; }

    public double getmNonWorkDay() { return mNonWorkDay; }
    public void setmNonWorkDay(double nonWorkDay) { this.mNonWorkDay = nonWorkDay; }

    //
    @Exclude
    public Date getNextPay() { return mNextPay; }
    @Exclude
    public final void setNextPay(Date mNextPay){ this.mNextPay = mNextPay; }

    @Exclude
    public Date getTimeStart() {
        return mTimeStart;
    }
    @Exclude
    public final void setTimeStart(Date mTimeStart) {
        this.mTimeStart = mTimeStart;
    }

    @Exclude
    public Date getTimeEnd() {
        return mTimeEnd;
    }
    @Exclude
    public final void setTimeEnd(Date mTimeEnd) {
        this.mTimeEnd = mTimeEnd;
    }

    public void cryptLogin() throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        mLogin = Crypt(mLogin);
    }
    public void cryptPassword() throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        mPassword = Crypt(mPassword);
    }

    private String Crypt(String unCrypt) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final byte[] bytes = unCrypt.getBytes("UTF-8");
        final byte[] keyBytes = uid.getBytes("UTF-8");

        SecretKeySpec sks = new SecretKeySpec(keyBytes, "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.ENCRYPT_MODE, sks);

        return new String(Base64.encode(cipher.doFinal(bytes), Base64.DEFAULT));
    }
}
