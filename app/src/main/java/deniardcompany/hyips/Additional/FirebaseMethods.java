package deniardcompany.hyips.Additional;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import deniardcompany.hyips.ActivityMain;
import deniardcompany.hyips.R;
import deniardcompany.hyips.RecyclerView.ViewHolder;

import static deniardcompany.hyips.Additional.ApplicationProgram.curTable;
import static deniardcompany.hyips.Additional.ApplicationProgram.mContext;
import static deniardcompany.hyips.Additional.ApplicationProgram.mDatabase;

/**
 * Created by LeBIIIa on 30.07.2016.
 */

public class FirebaseMethods {
    public static void addItem(Hyip hyip){
        addItem(hyip, curTable);
    }
    public static void addItem(Hyip hyip, String mTable){
        mDatabase.child(getUid()).child(mTable).child(hyip.getmId()).setValue(hyip);
    }
    public static void modifyItem(Hyip hyip){
        modifyItem(hyip, curTable);
    }
    public static void modifyItem(Hyip hyip, String mTable){
        String user = getUid();
        String key = hyip.getmId();
        Map<String, Object> changed = hyip.toMap();
        Map<String, Object> updated = new HashMap<>();
        updated.put("/" + user + "/" + mTable + "/" + key, changed);
        mDatabase.updateChildren(updated);
    }
    public static void deleteItem(String mId){
        deleteItem(mId, curTable);
    }
    public static void deleteItem(String mId, String mTable){
        mDatabase.child(getUid()).child(mTable).child(mId).removeValue();
    }

    public static String getUid(){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        return auth.getCurrentUser().getUid();
    }
}
