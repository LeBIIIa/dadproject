package deniardcompany.hyips.ContextActivity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;

import deniardcompany.hyips.Additional.ApplicationProgram;
import deniardcompany.hyips.Additional.Hyip;
import deniardcompany.hyips.Additional.SubHyip;
import deniardcompany.hyips.R;

import static deniardcompany.hyips.Additional.Methods.Update;

/**
 * Created by EPS on 10.05.2016.
 */
public class ActivityShowMoreItem extends AppCompatActivity  {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_more);
        Bundle extras = getIntent().getExtras();
        Hyip temp = (Hyip) extras.getSerializable(getString(R.string.hyip));
        SubHyip hyip = new SubHyip(temp);
        try {
            Update(hyip);
        } catch (Exception e) {
            finish();
            e.printStackTrace();
        }
        ApplicationProgram.hyip = hyip;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("Подробнее о проекте");

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Осн. информация"));
        tabLayout.addTab(tabLayout.newTab().setText("Доп. информация"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final TabPager adapter = new TabPager(getSupportFragmentManager(), 2);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}
            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

    @Override
    public void onBackPressed(){
        finish();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
