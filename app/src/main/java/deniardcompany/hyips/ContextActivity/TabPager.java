package deniardcompany.hyips.ContextActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by LeBIIIa on 15.05.2016.
 */
public class TabPager extends FragmentStatePagerAdapter {

    private int tabCount;

    public TabPager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount= tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragmentMainInfo tab1 = new FragmentMainInfo();
                return tab1;
            case 1:
                FragmentAddInfo tab2 = new FragmentAddInfo();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}