package deniardcompany.hyips;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;


import deniardcompany.hyips.Additional.Hyip;
import deniardcompany.hyips.ContextActivity.ActivityShowMoreItem;
import deniardcompany.hyips.Login.ActivityLogin;
import deniardcompany.hyips.RecyclerView.ItemAdapter;
import deniardcompany.hyips.RecyclerView.ViewHolder;
import deniardcompany.hyips.RecyclerView.DividerItemDecoration;

import static deniardcompany.hyips.Additional.ApplicationProgram.CHANGE;
import static deniardcompany.hyips.Additional.ApplicationProgram.SHOW;
import static deniardcompany.hyips.Additional.ApplicationProgram.curTable;
import static deniardcompany.hyips.Additional.ApplicationProgram.mDatabase;


public class ActivityMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView vTable;
    private FirebaseUser mUser;
    private Menu menu;
    private ItemAdapter eAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUser = FirebaseAuth.getInstance().getCurrentUser();


        AdView mAdView = (AdView) findViewById(R.id.mainAd);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        mAdView.loadAd(adRequest);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        ((TextView)header.findViewById(R.id.email)).setText(mUser.getEmail());
        header.findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(ActivityMain.this, ActivityLogin.class));
                finish();
            }
        });

        if ( savedInstanceState != null ){
            ((TextView)findViewById(R.id.allDeposit)).setText(savedInstanceState.getString("Deposits"));
            ((TextView)findViewById(R.id.allBack)).setText(savedInstanceState.getString("Back"));
            ((TextView)findViewById(R.id.difference)).setText(savedInstanceState.getString("Difference"));
            String temp = savedInstanceState.getString("Difference");
            temp = temp.substring(0, temp.length() - 1);
            double diff = Double.parseDouble(temp.replace(",", "."));
            if ( Double.compare(diff, 0.00f) > 0 ){
                ((TextView)findViewById(R.id.difference)).setTextColor(ContextCompat.getColor(this, R.color.Plus));
            }else if ( Double.compare(diff, 0.00f) < 0 ) {
                ((TextView)findViewById(R.id.difference)).setTextColor(ContextCompat.getColor(this, R.color.ToolbarMinus));
            }
            curTable = savedInstanceState.getString("curTable");
            if ( curTable.equals(getString(R.string.scam)) ){
                findViewById(R.id.difference).setVisibility(View.VISIBLE);
                findViewById(R.id.main).setVisibility(View.GONE);
            }
        }else{
            ((TextView)findViewById(R.id.allDeposit)).setText("Вложено: 0.00$");
            ((TextView)findViewById(R.id.allBack)).setText("Возвращено: 0.00$");
            ((TextView)findViewById(R.id.difference)).setText("0.00$");
            curTable = getString(R.string.hyip);
        }

        eAdapter = new ItemAdapter(Hyip.class, R.layout.item_adapter, ViewHolder.class, mDatabase.child(mUser.getUid()).child(curTable), this);
        vTable = (RecyclerView) findViewById(R.id.listProjects);
        vTable.setLayoutManager(new LinearLayoutManager(this));
        vTable.setItemAnimator(new DefaultItemAnimator());
        vTable.setHasFixedSize(true);
        vTable.addItemDecoration(new DividerItemDecoration(this));
        vTable.setAdapter(eAdapter);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putString("Deposits", ((TextView)findViewById(R.id.allDeposit)).getText().toString());
        outState.putString("Back", ((TextView)findViewById(R.id.allBack)).getText().toString());
        outState.putString("curTable", curTable);
        outState.putString("Difference",((TextView)findViewById(R.id.difference)).getText().toString());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        this.menu = menu;
        return true;
    }

    //Button Update
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.menuAdd) {
            Intent intent = new Intent(ActivityMain.this, ActivityAdd.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Navigation Bar
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if ( id == R.id.active ){
            curTable = getString(R.string.hyip);

            eAdapter = new ItemAdapter(Hyip.class, R.layout.item_adapter, ViewHolder.class, mDatabase.child(mUser.getUid()).child(curTable), this);
            vTable.swapAdapter(eAdapter, true);

            findViewById(R.id.main).setVisibility(View.VISIBLE);
            findViewById(R.id.difference).setVisibility(View.GONE);
        }
        if ( id == R.id.archive ){
            curTable = getString(R.string.scam);

            eAdapter = new ItemAdapter(Hyip.class, R.layout.item_adapter, ViewHolder.class, mDatabase.child(mUser.getUid()).child(curTable), this);
            vTable.swapAdapter(eAdapter, true);

            findViewById(R.id.main).setVisibility(View.GONE);
            findViewById(R.id.difference).setVisibility(View.VISIBLE);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        eAdapter.getItemSelected(item);
        return super.onContextItemSelected(item);
    }

    public void showActivity(int code, Intent intent, Hyip hyip){
        if ( code == CHANGE ){
            intent = new Intent(ActivityMain.this, ActivityAdd.class);
        }
        if ( code == SHOW ){
            intent = new Intent(ActivityMain.this, ActivityShowMoreItem.class);
        }
        intent.putExtra(getString(R.string.hyip), hyip);
        startActivity(intent);
    }

}
