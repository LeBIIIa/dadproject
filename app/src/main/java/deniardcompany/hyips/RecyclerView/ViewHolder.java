package deniardcompany.hyips.RecyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import deniardcompany.hyips.R;

import static deniardcompany.hyips.Additional.ApplicationProgram.CHANGE;
import static deniardcompany.hyips.Additional.ApplicationProgram.DELETE;
import static deniardcompany.hyips.Additional.ApplicationProgram.SHOW;
import static deniardcompany.hyips.Additional.ApplicationProgram.TOARCHIVE;
import static deniardcompany.hyips.Additional.ApplicationProgram.curTable;


/**
 * Created by LeBIIIa on 30.05.2016.
 */

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
    public ImageView sysPayment;
    public TextView name;
    public TextView percent;
    public TextView depositBack;
    public TextView startDeposit;
    public TextView endDeposit;
    public TextView deposit;

    public ViewHolder(View v) {
        super(v);
        sysPayment = (ImageView)v.findViewById(R.id.sysPayment);
        name = (TextView)v.findViewById(R.id.name);
        percent = (TextView)v.findViewById(R.id.percent);
        depositBack = (TextView)v.findViewById(R.id.back);
        startDeposit = (TextView)v.findViewById(R.id.start);
        endDeposit = (TextView)v.findViewById(R.id.end);
        deposit = (TextView)v.findViewById(R.id.deposit);
        v.setOnCreateContextMenuListener(this);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (curTable.equals(v.getResources().getString(R.string.hyip))) {
            menu.add(0, SHOW, 0, "Подробнее");
            menu.add(0, CHANGE, 0, "Изменить данные");
            menu.add(0, TOARCHIVE, 0, "Переместить в архив");
        } else {
            menu.add(0, CHANGE, 0, "Изменить данные");
            menu.add(0, DELETE, 0, "Удалить с архива");
        }
    }
}
