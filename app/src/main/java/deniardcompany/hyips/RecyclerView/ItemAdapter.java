package deniardcompany.hyips.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import deniardcompany.hyips.Additional.FirebaseMethods;
import deniardcompany.hyips.Additional.Hyip;
import deniardcompany.hyips.ActivityMain;
import deniardcompany.hyips.Additional.Methods;
import deniardcompany.hyips.R;

import static deniardcompany.hyips.Additional.ApplicationProgram.CHANGE;
import static deniardcompany.hyips.Additional.ApplicationProgram.DELETE;
import static deniardcompany.hyips.Additional.ApplicationProgram.SHOW;
import static deniardcompany.hyips.Additional.ApplicationProgram.TOARCHIVE;
import static deniardcompany.hyips.Additional.ApplicationProgram.curTable;
import static deniardcompany.hyips.Additional.Methods.nullableValue;
import static deniardcompany.hyips.Additional.Methods.updateValue;


/**
 * Created by LeBIIIa on 25.04.2016.
 */
public class ItemAdapter extends FirebaseRecyclerAdapter<Hyip, ViewHolder>  {

    private Context mContext;
    private int selectedId;

    public ItemAdapter(Class<Hyip> modelClass, int modelLayout, Class<ViewHolder> viewHolderClass, DatabaseReference ref, Context mContext) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.mContext = mContext;
    }

    @SuppressLint("LongLogTag")
    @Override
    protected Hyip parseSnapshot(DataSnapshot snapshot) {
        Log.d("Er", "parse");
        Hyip hyip = snapshot.getValue(Hyip.class);
        if ( hyip.getmId().compareTo(getRef(0).getKey()) == 0 )
            nullableValue();
        updateValue(hyip.getmDeposit(), hyip.getmBack());
        if ( curTable.equals(mContext.getString(R.string.hyip))  ){
            if ( hyip.getTimeEnd().before(DateTime.now().toDate()) ){
                FirebaseMethods.deleteItem(hyip.getmId(), curTable);
                FirebaseMethods.addItem(hyip, mContext.getString(R.string.scam));
            }
            if ( hyip.getNextPay().equals(new LocalDate().toDate()) ){
                try {
                    Methods.Update(hyip);
                    FirebaseMethods.modifyItem(hyip);
                } catch (Exception e) {
                    Log.e("ParseSnapshot(ItemAdapter)", "ERROR");
                    e.printStackTrace();
                }
            }
        }

        return hyip;
    }

    @Override
    protected void populateViewHolder(ViewHolder viewHolder, Hyip model, int position) {
        if ( curTable.equals(mContext.getString(R.string.hyip)) ){
            hyip(viewHolder, model, position);
        }else if (curTable.equals(mContext.getString(R.string.scam))) {
            scam(viewHolder, model, position);
        }
    }

    private void hyip(ViewHolder holder, Hyip hyip,final int position){
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                selectedId = position;
                return false;
            }
        });
        holder.name.setText(hyip.getmNameHyip());
        holder.deposit.setText(String.valueOf(hyip.getmDeposit()));
        if ( hyip.getmCountPay() != 0 )
            holder.percent.setText(String.valueOf(hyip.getmPercentPlan()));
        else{
            String str = "Пн-Пт: " + hyip.getmWorkDay() + "$";
            str += "\n";
            str += "Сб-Нд: " +  hyip.getmNonWorkDay() + "$";
            holder.percent.setText(str);
        }

        if ( Double.compare(hyip.getmRefBack(), 0.0f) > 0 )
            holder.depositBack.setText(String.format("%.2f",hyip.getmBack() - hyip.getmRefBack()) + " + " + String.format("%.2f",hyip.getmRefBack()));
        else
            holder.depositBack.setText(String.format("%.2f",hyip.getmBack()));

        if ( hyip.getmBack() + hyip.getmRefBack() > hyip.getmDeposit() )
            holder.depositBack.setTextColor(ContextCompat.getColor(mContext, R.color.Plus));
        else
            holder.depositBack.setTextColor(ContextCompat.getColor(mContext, R.color.Minus));

        setSysPayment(holder.sysPayment, hyip.getmSysPayment());

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", new Locale("ru", "RU"));

        holder.startDeposit.setText(sdf.format(hyip.getmTimeStart()));
        holder.endDeposit.setText(sdf.format(hyip.getmTimeEnd()));


        LocalDate localDate = new LocalDate(hyip.getmTimeEnd());
        if ( localDate.getYear() == 9999 ){
            holder.endDeposit.setText(mContext.getString(R.string.inf));
        }
    }

    private void scam(ViewHolder holder, Hyip hyip,final int position){
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                selectedId = position;
                return false;
            }
        });
        holder.name.setText(hyip.getmNameHyip());
        holder.deposit.setText(String.valueOf(hyip.getmDeposit()));
        holder.percent.setText(String.valueOf(hyip.getmPercentPlan()));

        holder.depositBack.setText(String.format("%.2f", hyip.getmBack() - hyip.getmRefBack()));

        if ( Double.compare(hyip.getmRefBack(), 0.0f) > 0 ){
            String temp = holder.depositBack.getText().toString();
            holder.depositBack.setText(temp + " + " + String.format("%.2f", hyip.getmRefBack()));
        }
        if ( hyip.getmBack() + hyip.getmRefBack() > hyip.getmDeposit() )
            holder.depositBack.setTextColor(ContextCompat.getColor(mContext, R.color.Plus));
        else
            holder.depositBack.setTextColor(ContextCompat.getColor(mContext, R.color.Minus));

        setSysPayment(holder.sysPayment, hyip.getmSysPayment());

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", new Locale("ru", "RU"));

        holder.startDeposit.setText(sdf.format(hyip.getmTimeStart()));
        holder.endDeposit.setText(sdf.format(hyip.getmTimeEnd()));


        LocalDate localDate = new LocalDate(hyip.getmTimeEnd());
        if ( localDate.getYear() == 9999 ){
            holder.endDeposit.setText(mContext.getString(R.string.inf));
        }
    }


    @Override
    public void onViewRecycled(ViewHolder holder){
        holder.itemView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

    public void getItemSelected(MenuItem item){
        Hyip hyip = this.getItem(selectedId);
        Intent intent = null;
        switch (item.getItemId()){
            case SHOW:
                ((ActivityMain)mContext).showActivity(SHOW, intent, hyip);
                break;
            case CHANGE:
                ((ActivityMain)mContext).showActivity(CHANGE, intent, hyip);
                break;
            case TOARCHIVE:
                FirebaseMethods.deleteItem(hyip.getmId());
                FirebaseMethods.addItem(hyip, mContext.getString(R.string.scam));
                break;
            case DELETE:
                FirebaseMethods.deleteItem(hyip.getmId());
                break;
        }
    }

    private void setSysPayment(ImageView mSysPayment, String sysPayment){
        Resources resources = mContext.getResources();
        if ( sysPayment.equals(resources.getString(R.string.Adv)) )
            mSysPayment.setImageResource(R.drawable.advcash);
        else if ( sysPayment.equals(resources.getString(R.string.OK)) )
            mSysPayment.setImageResource(R.drawable.okpay);
        else if ( sysPayment.equals(resources.getString(R.string.PM)) )
            mSysPayment.setImageResource(R.drawable.perfectmoney);
        else if ( sysPayment.equals(resources.getString(R.string.BitCoin)) )
            mSysPayment.setImageResource(R.drawable.bitcoin);
        else if ( sysPayment.equals(resources.getString(R.string.MC)) )
            mSysPayment.setImageResource(R.drawable.mastercard);
        else if ( sysPayment.equals(resources.getString(R.string.Visa)) )
            mSysPayment.setImageResource(R.drawable.visa);
        else if ( sysPayment.equals(resources.getString(R.string.PP)) )
            mSysPayment.setImageResource(R.drawable.paypal);
        else if ( sysPayment.equals(resources.getString(R.string.WebMoney)) )
            mSysPayment.setImageResource(R.drawable.webmoney);
        else if ( sysPayment.equals(resources.getString(R.string.Qiwi)) )
            mSysPayment.setImageResource(R.drawable.qiwi);
        else if ( sysPayment.equals(resources.getString(R.string.YandexMoney)) )
            mSysPayment.setImageResource(R.drawable.yandexmoney);
        else if ( sysPayment.equals(resources.getString(R.string.Skrill)) )
            mSysPayment.setImageResource(R.drawable.skrill);
        else if ( sysPayment.equals(resources.getString(R.string.Neteller)) )
            mSysPayment.setImageResource(R.drawable.neteller);
        else if ( sysPayment.equals(resources.getString(R.string.Payza)) )
            mSysPayment.setImageResource(R.drawable.payza);
        else if ( sysPayment.equals(resources.getString(R.string.Payeer)) )
            mSysPayment.setImageResource(R.drawable.payeer);
        else if ( sysPayment.equals(resources.getString(R.string.NIX)) )
            mSysPayment.setImageResource(R.drawable.nixmoney);
    }

}
