package deniardcompany.hyips;

import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import deniardcompany.hyips.Additional.FirebaseMethods;
import deniardcompany.hyips.Additional.FragmentDialog;
import deniardcompany.hyips.Additional.Hyip;
import deniardcompany.hyips.Additional.Methods;
import fr.ganfra.materialspinner.MaterialSpinner;

import static deniardcompany.hyips.Additional.ApplicationProgram.curTable;
import static deniardcompany.hyips.Additional.Methods.deCrypt;


public class ActivityAdd extends AppCompatActivity {

    private EditText mNameHyip, mSite, mDeposit, mPercentPlan, mLogin, mPassword, mDate, mAge, mRefback, mBack, mProfit, mWorkDay, mNonWorkDay;
    private MaterialSpinner mCountPay, mSysPayment;
    private SimpleDateFormat sdf;

    private Hyip hyip;

    private String[] cPay, sysPayments;
    private int[] pays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_menu);

        hyip = null;

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);

        AdView mAdView = (AdView) findViewById(R.id.addAd);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        mAdView.loadAd(adRequest);

        init();
        initSpinner();
        ifHyip();

        Bundle extras = getIntent().getExtras();
        if ( extras != null ){
            hyip = (Hyip) extras.getSerializable(getString(R.string.hyip));
            actionBar.setTitle("Обновить вклад");
            fill();
        }

        mCountPay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if ( i == 0 ){
                    findViewById(R.id.Days).setVisibility(View.VISIBLE);
                    findViewById(R.id.percentPlan).setVisibility(View.GONE);
                }else{
                    findViewById(R.id.percentPlan).setVisibility(View.VISIBLE);
                    findViewById(R.id.Days).setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        //

        mSite.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if ( !s.toString().startsWith("http://") ){
                    mSite.setText("http://");
                    mSite.setSelection(7);
                }
            }
        });
        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentDialog dialog = new FragmentDialog().newInstance(mDate);
                dialog.show(getFragmentManager(), "AddItem");
            }
        });

    }

    //Button Update
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.addMenu || id == R.id.updateMenu) {
            try {
                String nameHyip = mNameHyip.getText().toString();
                if (nameHyip.isEmpty())
                    throw new Exception(String.valueOf(mNameHyip.getId()));

                String site = mSite.getText().toString();
                if ( site.equals("http://") )
                    throw new Exception(String.valueOf(mSite.getId()));

                String temp = mDeposit.getText().toString();
                double deposit = 0;
                if (temp.isEmpty())
                    throw new Exception(String.valueOf(mDeposit.getId()));
                deposit = Double.parseDouble(temp);

                int countPay = mCountPay.getSelectedItemPosition();
                if (countPay == 0)
                    throw new Exception(String.valueOf(mCountPay.getId()));
                countPay = pays[countPay - 1];

                double workDay = 0, nonWorkDay = 0, percentPlan = 0;
                if ( countPay == 0 ){
                    temp = mWorkDay.getText().toString();
                    if ( temp.isEmpty() )
                        throw new Exception(String.valueOf(mWorkDay.getId()));
                    workDay = Double.parseDouble(temp);

                    temp = mNonWorkDay.getText().toString();
                    if ( temp.isEmpty() )
                        throw new Exception(String.valueOf(mNonWorkDay.getId()));
                    nonWorkDay = Double.parseDouble(temp);
                }else {
                    temp = mPercentPlan.getText().toString();
                    if (temp.isEmpty())
                        throw new Exception(String.valueOf(mPercentPlan.getId()));
                    percentPlan = Double.parseDouble(temp);
                }

                int system = mSysPayment.getSelectedItemPosition();
                String sysPayment;
                if (system == 0)
                    throw new Exception(String.valueOf(mSysPayment.getId()));
                sysPayment = sysPayments[system - 1];

                temp = String.valueOf(mDate.getText());
                Date date;
                if (temp.isEmpty())
                    throw new Exception(String.valueOf(mDate.getId()));
                date = sdf.parse(temp);

                temp = String.valueOf(mAge.getText());
                int age = 0;
                if ( !temp.isEmpty() )
                    age = Integer.parseInt(temp);

                String login = mLogin.getText().toString();
                if (login.isEmpty()) {
                    login = "";
                }

                String pass = mPassword.getText().toString();
                if (pass.isEmpty()) {
                    pass = "";
                }

                temp = String.valueOf(mRefback.getText());
                double refback = 0;
                if ( !temp.isEmpty() )
                    refback = Double.parseDouble(temp);

                double back = 0, profit = 0;
                if ( curTable.equals(getString(R.string.scam)) ){
                    temp = mBack.getText().toString();
                    if ( !temp.isEmpty() )
                        back = Double.parseDouble(temp);
                    temp = mProfit.getText().toString();
                    if ( !temp.isEmpty() )
                        profit = Double.parseDouble(temp);
                }
                //Summary
                Hyip added = new Hyip(nameHyip, site, sysPayment, login, pass, date, percentPlan, countPay, workDay, nonWorkDay, age, deposit, refback, back, profit);
                Crypt(added);

                if ( curTable.equals(getString(R.string.hyip)) )
                    Methods.Update(added);

                if ( hyip != null ) {
                    added.setmId(hyip.getmId());
                    FirebaseMethods.modifyItem(added);
                }else
                    FirebaseMethods.addItem(added);

                finish();
            }catch (ParseException e) {
                Toast.makeText(this, "ParseExceptionAdd", Toast.LENGTH_SHORT).show();
            }catch (Exception e) {
                e.printStackTrace();
                gotException(e.getMessage());
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init(){
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        //
        mNameHyip = (EditText)findViewById(R.id.nameHyip);
        mSite = (EditText)findViewById(R.id.site);
        mDeposit = (EditText)findViewById(R.id.deposit);
        mPercentPlan = (EditText)findViewById(R.id.percentPlan);
        mLogin = (EditText)findViewById(R.id.login);
        mPassword = (EditText)findViewById(R.id.password);
        mDate = (EditText)findViewById(R.id.date);
        mWorkDay = (EditText)findViewById(R.id.mWorkDay);
        mNonWorkDay = (EditText)findViewById(R.id.mNonWorkDay);
        mAge = (EditText)findViewById(R.id.age);
        mRefback = (EditText)findViewById(R.id.refBack);
        mProfit = (EditText)findViewById(R.id.profit);
        mBack = (EditText) findViewById(R.id.back);
        //
        mCountPay = (MaterialSpinner)findViewById(R.id.countPay);
        mSysPayment = (MaterialSpinner)findViewById(R.id.sysPayment);
        //
        mSite.setText("http://");
        mSite.setSelection(7);
    }
    private void initSpinner(){
        cPay = getResources().getStringArray(R.array.PAYS);
        sysPayments = getResources().getStringArray(R.array.SYSTEM);
        pays = getResources().getIntArray(R.array.intPays);


        ArrayAdapter<String> pays = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cPay);
        ArrayAdapter<String> payments = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sysPayments);

        mCountPay.setAdapter(pays);
        mSysPayment.setAdapter(payments);
    }
    private void ifHyip(){
        if ( curTable.equals(getString(R.string.hyip)) ) {
            mBack.setFocusable(false);
            mBack.setEnabled(false);
            mBack.setCursorVisible(false);
            mBack.setKeyListener(null);
            mBack.setText("Доступно только в архиве!");
            mProfit.setFocusable(false);
            mProfit.setEnabled(false);
            mProfit.setCursorVisible(false);
            mProfit.setKeyListener(null);
            mProfit.setText("Доступно только в архиве!");
        }
    }

    private void fill(){
        mNameHyip.setText(hyip.getmNameHyip());
        mSite.setText(hyip.getmSiteHyip());
        mDeposit.setText(String.valueOf(hyip.getmDeposit()));
        mPercentPlan.setText(String.valueOf(hyip.getmPercentPlan()));
        mLogin.setText(hyip.getmLogin());
        mPassword.setText(hyip.getmPassword());
        mDate.setText(sdf.format(hyip.getmTimeStart()));
        mAge.setText(String.valueOf((long)hyip.getmAgeDeposit()));
        mRefback.setText(String.valueOf(hyip.getmRefBack()));
        mPercentPlan.setText(String.valueOf(hyip.getmPercentPlan()));

        if ( curTable.equals(getString(R.string.scam)) ){
            mProfit.setText(String.valueOf(hyip.getmProfit()));
            mBack.setText(String.valueOf(hyip.getmBack()));
        }

        switch (hyip.getmCountPay()){
            case 0:
                mCountPay.setSelection(1);
                mWorkDay.setText(String.valueOf(hyip.getmWorkDay()));
                mNonWorkDay.setText(String.valueOf(hyip.getmNonWorkDay()));
                break;
            case 1:
                mCountPay.setSelection(2);
                break;
            case 7:
                mCountPay.setSelection(3);
                break;
            case 30:
                mCountPay.setSelection(4);
                break;
            case 31:
                mCountPay.setSelection(5);
                break;
            default:
                mCountPay.setSelection(0);
        }
        String sysPayment = hyip.getmSysPayment();
        for ( int i = 0;i<sysPayments.length;++i ){
            if ( sysPayment.equals(sysPayments[i]) ){
                mSysPayment.setSelection(i + 1);
                break;
            }
        }

        try {
            mLogin.setText(deCrypt(hyip.getmLogin()));
            mPassword.setText(deCrypt(hyip.getmPassword()));
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void gotException(String message){
        try{
            int id = Integer.parseInt(message);
            switch (id) {
                case R.id.nameHyip:
                    mNameHyip.setError("Введите название проекта!");
                    break;
                case R.id.site:
                    mSite.setError("Введите адрес сайта!");
                    break;
                case R.id.deposit:
                    mDeposit.setError("Введите сумму вклада!");
                    break;
                case R.id.percentPlan:
                    mPercentPlan.setError("Введите процентный план!");
                    break;
                case R.id.date:
                    mDate.setError("Выберите дату начала вклада!");
                    break;
                case R.id.mWorkDay:
                    mWorkDay.setError("Введите сумму начислений в рабочие дни!");
                    break;
                case R.id.mNonWorkDay:
                    mNonWorkDay.setError("Введите сумму начислений в выходные дни!");
                    break;
                case R.id.countPay:
                    mCountPay.setError("Выберите выплату!");
                    break;
                case R.id.sysPayment:
                    mSysPayment.setError("Выберите систему платежей!");
                    break;
                default:
                    Toast.makeText(this, "gotException()", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if ( hyip == null )
            getMenuInflater().inflate(R.menu.addmenu, menu);
        else
            getMenuInflater().inflate(R.menu.updatemenu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void Crypt(Hyip hyip){
        try {
            hyip.cryptLogin();
            hyip.cryptPassword();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
